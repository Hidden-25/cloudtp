terraform {
required_version = ">= 0.14.0"
  required_providers {
    openstack = {
      source  = "terraform-provider-openstack/openstack"
      version = "~> 1.47.0"
    }
  }
}

# Configure the OpenStack Provider
provider "openstack" {
  user_name         = "najacquet"
  tenant_name       = "iut_prj_najacquet"
  password          = "${var.password}"
  auth_url          = "https://openstack.iut.uca.fr:5000"
  region            = "RegionOne"
  user_domain_name  = "UCAFR"
  project_domain_id = "default"
}

# Configure the OpenStack securitygroup
resource "openstack_compute_secgroup_v2" "secgroup_1" {
  name = "secgroup"
  description = "my security group"

rule {
    from_port   = 22
    to_port     = 22
    ip_protocol = "tcp"
    cidr        = "0.0.0.0/0"
  }
}

# Configure the OpenStack network
resource "openstack_networking_network_v2" "network_1" {
  name ="network_1"
  admin_state_up = "true"
  }

# Configure the OpenStack subnet  
resource "openstack_networking_subnet_v2" "subnet_1" {
  name       = "subnet_1"
  network_id = "${openstack_networking_network_v2.network_1.id}"
  cidr       = "10.0.0.0/24"
  ip_version = 4
}

# Configure the OpenStack router
resource "openstack_networking_router_v2" "router_1" {
  name                = "my_router"
  external_network_id = "01dad3d2-8b27-4fa7-8ea3-4488945f2467"
}

resource "openstack_networking_router_interface_v2" "router_interface_1" {
  router_id = "${openstack_networking_router_v2.router_1.id}"
  subnet_id = "${openstack_networking_subnet_v2.subnet_1.id}"
}

resource "openstack_networking_floatingip_v2" "fip_1" {
  pool = "iut_netpub"
}

# Configure the OpenStack instance
resource "openstack_compute_instance_v2" "moninstance" {
  name = "moninstance"
  image_id = "fdfa51a1-03a8-4a18-9fbb-e71f771b2822"
  flavor_name = "m1_small"
  key_pair = "clessh"
  security_groups = ["default","secgroup"]

  network {
    name = "network_1"
  }
  depends_on = [openstack_compute_secgroup_v2.secgroup_1,openstack_networking_network_v2.network_1]
}

resource "openstack_compute_floatingip_associate_v2" "fip_1" {
  floating_ip = "${openstack_networking_floatingip_v2.fip_1.address}"
  instance_id = "${openstack_compute_instance_v2.moninstance.id}"
}


